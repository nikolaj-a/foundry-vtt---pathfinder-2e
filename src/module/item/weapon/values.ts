/** Precious materials that provide effects to strike attack or damage rolls */
const WEAPON_MATERIAL_EFFECTS = new Set([
    "abysium",
    "adamantine",
    "coldIron",
    "djezet",
    "silver",
    "mithral",
    "noqual",
    "peachwood",
    "silver",
    "sovereignSteel",
] as const);

export { WEAPON_MATERIAL_EFFECTS };
